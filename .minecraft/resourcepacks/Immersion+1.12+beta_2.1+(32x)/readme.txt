Immersion 1.12 beta_2.1 (32x), released 9/28/2020

Created by Quin

Special Thanks to L "Figuren" H for his early patronage.

Inspiration for Immersion textures comes from the classic Minecraft textures, courtesy of Mojang AB. This pack is in no way affiliated with Mojang and any questions or comments
regarding the pack should not be brought to Mojang, but should rather be directed towards me, the creator, either personally or at
Quin Studios.

YOU MAY:

	� Use Immersion and its assets for videos. This includes recorded footage (Let's plays, trailers, etc.) as well as animations or other forms of video that do not directly use the pack, but you must credit me (as either Quin or Quin Studios) in the description
	� Use and change any non-texture graphical works related to Immersion, so long as you give credit.
	� Customize/edit Immersion, so long as you do not distribute it. You may still upload videos and screenshots using modified textures, but you must give me credit, and note that you modified the textures. Even though you might be the author of the modified textures, you still have no authorship rights over them, because the original textures are not your own work.

YOU MAY NOT:

	� Share only the download link to the resourcepack. If you want to share the resource pack with anyone, you must post a link to one of my respective pack pages (currently only the www.planetminecraft.com page)
	� Re-upload the resourcepack or any part of it without permission.
	� Republish any Immersion threads or pages without permission. You may make translations on other pages, however, as long as you leave the original links, and notify me that you are doing so. Try not to ask to republish anywhere where I have already published the pack, as that will not be allowed.
	� Use any content from the resource pack for anything other than the things mentioned in this document without permission.
	� Remix the resource pack without permission, as that would be a re-upload.

If you have any questions, you can contact me at quinstudios@gmail.com