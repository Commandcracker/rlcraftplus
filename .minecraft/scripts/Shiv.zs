import crafttweaker.item.IItemStack;
import crafttweaker.item.IIngredient;
import mods.jei.JEI;

print("Script starting!");

<ore:toolAxe>.add(<iceandfire:silver_axe:*>);
<ore:toolAxe>.add(<iceandfire:dragonbone_axe:*>);

var itemArray as IItemStack[] = [
  # Default
  <spartanshields:shield_abyssalcraft_darkstone>,
  <variedcommodities:full_wooden_boots>,
  <variedcommodities:wooden_dagger>,
  <variedcommodities:iron_trident>,
  <mujmajnkraftsbettersurvival:itemsteelhammer>,
  <variedcommodities:pendant>,
  <variedcommodities:demonic_skirt>,
  <variedcommodities:bear_claw>,
  <spartanweaponry:rapier_copper>,
  <variedcommodities:spell_lightning>,
  <variedcommodities:frost_sword>,
  <variedcommodities:crowbar>,
  <variedcommodities:iron_shield>,
  <mujmajnkraftsbettersurvival:itemcoppernunchaku>,
  <variedcommodities:bronze_glaive>,
  <variedcommodities:spell_dark>,
  <mujmajnkraftsbettersurvival:itemaluminiumbattleaxe>,
  <spartanweaponry:mace_platinum>,
  <variedcommodities:bronze_mace>,
  <spartanweaponry:saber_platinum>,
  <variedcommodities:kunai>,
  <variedcommodities:paper_crown>,
  <variedcommodities:carpentry_bench:1>,
  <spartanweaponry:rapier_steel>,
  <variedcommodities:demonic_scythe>,
  <variedcommodities:orb>,
  <variedcommodities:soldier_chest>,
  <variedcommodities:necklace>,
  <variedcommodities:mithril_battleaxe>,
  <variedcommodities:stone_dagger>,
  <variedcommodities:ninja_claw>,
  <variedcommodities:emerald_dagger>,
  <variedcommodities:x407_head>,
  <spartanweaponry:katana_tin>,
  <variedcommodities:batton>,
  <spartanweaponry:greatsword_steel>,
  <variedcommodities:bronze_shield>,
  <variedcommodities:wizard_head>,
  <spartanweaponry:glaive_nickel>,
  <variedcommodities:statuette>,
  <spartanweaponry:longsword_electrum>,
  <spartanweaponry:glaive_steel>,
  <variedcommodities:locket>,
  <spartanweaponry:spear_nickel>,
  <variedcommodities:demonic_staff>,
  <mujmajnkraftsbettersurvival:itemcopperhammer>,
  <spartanweaponry:glaive_electrum>,
  <spartanweaponry:hammer_steel>,
  <variedcommodities:commissar_bottom>,
  <variedcommodities:stone_battleaxe>,
  <variedcommodities:demonic_shield_round>,
  <spartanshields:shield_tower_steel>,
  <spartanweaponry:longsword_steel>,
  <spartanweaponry:glaive_tin>,
  <spartanweaponry:glaive_copper>,
  <spartanweaponry:battleaxe_tin>,
  <variedcommodities:commissar_head>,
  <spartanshields:shield_botania_terrasteel>,
  <spartanweaponry:javelin_steel>,
  <spartanshields:shield_basic_tin>,
  <mujmajnkraftsbettersurvival:itemcopperdagger>,
  <variedcommodities:wooden_gun>,
  <variedcommodities:iron_glaive>,
  <spartanweaponry:dagger_platinum>,
  <spartanweaponry:pike_platinum>,
  <spartanshields:shield_riot_enderio>,
  <variedcommodities:broken_bottle>,
  <variedcommodities:iron_spear>,
  <variedcommodities:coin_wood>,
  <grapplemod:doubleupgradeitem>,
  <spartanshields:shield_riot_rftools>,
  <variedcommodities:full_bronze_head>,
  <variedcommodities:bronze_dagger_reversed>,
  <variedcommodities:bronze_battleaxe>,
  <spartanweaponry:katana_nickel>,
  <spartanweaponry:rapier_nickel>,
  <spartanweaponry:throwing_axe_electrum>,
  <spartanshields:shield_basic_copper>,
  <spartanweaponry:battleaxe_copper>,
  <spartanshields:shield_tower_tin>,
  <spartanweaponry:pike_steel>,
  <variedcommodities:iron_mace>,
  <variedcommodities:mithril_broadsword>,
  <variedcommodities:heater_shield>,
  <variedcommodities:campfire>,
  <mujmajnkraftsbettersurvival:itemsignalumspear>,
  <variedcommodities:emerald_gun>,
  <mujmajnkraftsbettersurvival:iteminvarnunchaku>,
  <mujmajnkraftsbettersurvival:itemlumiumbattleaxe>,
  <variedcommodities:emerald_spear>,
  <spartanweaponry:hammer_platinum>,
  <mujmajnkraftsbettersurvival:itemenderiumnunchaku>,
  <spartanweaponry:boomerang_tin>,
  <spartanweaponry:mace_steel>,
  <grapplemod:longfallboots>,
  <variedcommodities:full_bronze_chest>,
  <variedcommodities:mithril_glaive>,
  <grapplemod:launcheritem>,
  <spartanweaponry:longbow_invar>,
  <variedcommodities:mithril_boots>,
  <variedcommodities:crystal_block>,
  <variedcommodities:full_emerald_legs>,
  <mujmajnkraftsbettersurvival:itemcopperspear>,
  <variedcommodities:full_leather_head>,
  <spartanshields:shield_basic_soulforged_steel>,
  <spartanweaponry:katana_copper>,
  <spartanweaponry:spear_tin>,
  <mujmajnkraftsbettersurvival:itemsteeldagger>,
  <variedcommodities:wooden_trident>,
  <variedcommodities:ninja_head>,
  <spartanweaponry:lance_electrum>,
  <variedcommodities:golden_shield>,
  <spartanweaponry:boomerang_steel>,
  <spartanweaponry:halberd_invar>,
  <variedcommodities:wooden_warhammer>,
  <variedcommodities:wooden_staff>,
  <spartanweaponry:greatsword_lead>,
  <variedcommodities:golden_warhammer>,
  <variedcommodities:bronze_gun>,
  <variedcommodities:golden_halberd>,
  <variedcommodities:golden_dagger_reversed>,
  <variedcommodities:mithril_legs>,
  <spartanweaponry:dagger_tin>,
  <variedcommodities:crescent_shield>,
  <spartanweaponry:saber_nickel>,
  <variedcommodities:full_emerald_chest>,
  <spartanshields:shield_basic_steel>,
  <spartanweaponry:throwing_knife_platinum>,
  <spartanweaponry:crossbow_steel>,
  <variedcommodities:baseball_bat>,
  <variedcommodities:demonic_glaive>,
  <spartanweaponry:spear_invar>,
  <variedcommodities:bronze_staff>,
  <spartanweaponry:staff_electrum>,
  <variedcommodities:tuxedo_bottom>,
  <variedcommodities:stone_trident>,
  <variedcommodities:french_horn>,
  <spartanshields:shield_abyssalcraft_coralium>,
  <variedcommodities:bronze_skirt>,
  <variedcommodities:mithril_scythe>,
  <spartanweaponry:battleaxe_nickel>,
  <spartanshields:shield_abyssalcraft_dreadium>,
  <spartanweaponry:mace_copper>,
  <grapplemod:baseupgradeitem>,
  <variedcommodities:diamond_dagger_reversed>,
  <buildcraftsilicon:advanced_crafting_table>,
  <variedcommodities:iron_broadsword>,
  <variedcommodities:gem_sapphire>,
  <spartanweaponry:lance_platinum>,
  <variedcommodities:ancient_scroll>,
  <variedcommodities:diamond_halberd>,
  <spartanweaponry:boomerang_platinum>,
  <spartanweaponry:throwing_axe_steel>,
  <spartanweaponry:halberd_platinum>,
  <variedcommodities:full_wooden_head>,
  <variedcommodities:tactical_head>,
  <variedcommodities:golden_spear>,
  <variedcommodities:katar>,
  <variedcommodities:full_golden_head>,
  <spartanweaponry:crossbow_copper>,
  <mujmajnkraftsbettersurvival:itemsteelbattleaxe>,
  <spartanweaponry:lance_copper>,
  <variedcommodities:frost_shield>,
  <variedcommodities:cow_leather_head>,
  <spartanweaponry:throwing_knife_invar>,
  <mujmajnkraftsbettersurvival:itemenderiumhammer>,
  <variedcommodities:broken_arrow>,
  <variedcommodities:bullet>,
  <variedcommodities:frost_staff>,
  <variedcommodities:slingshot>,
  <variedcommodities:golden_scythe>,
  <mujmajnkraftsbettersurvival:itemsignalumdagger>,
  <variedcommodities:assassin_boots>,
  <variedcommodities:cursed_trident>,
  <spartanshields:shield_basic_platinum>,
  <mujmajnkraftsbettersurvival:itemlumiumhammer>,
  <variedcommodities:cow_leather_chest>,
  <spartanweaponry:battleaxe_steel>,
  <mujmajnkraftsbettersurvival:itemsteelspear>,
  <variedcommodities:macuahuitl>,
  <spartanweaponry:crossbow_lead>,
  <spartanweaponry:halberd_steel>,
  <variedcommodities:iron_warhammer>,
  <spartanweaponry:saber_invar>,
  <variedcommodities:stone_broadsword>,
  <variedcommodities:element_air>,
  <variedcommodities:demonic_head>,
  <variedcommodities:full_iron_chest>,
  <spartanweaponry:spear_electrum>,
  <variedcommodities:bo_staff>,
  <variedcommodities:wooden_shield_round>,
  <grapplemod:throwupgradeitem>,
  <spartanweaponry:longsword_nickel>,
  <spartanweaponry:mace_electrum>,
  <spartanweaponry:greatsword_copper>,
  <variedcommodities:golden_trident>,
  <variedcommodities:shuriken>,
  <spartanweaponry:mace_nickel>,
  <spartanweaponry:lance_tin>,
  <variedcommodities:crossbow_bolt>,
  <variedcommodities:wooden_battleaxe>,
  <variedcommodities:crown2>,
  <spartanweaponry:katana_steel>,
  <variedcommodities:diamond_glaive>,
  <variedcommodities:leather_skirt>,
  <variedcommodities:leaf_sword>,
  <mujmajnkraftsbettersurvival:itemelectrumhammer>,
  <variedcommodities:spell_arcane>,
  <variedcommodities:demonic_chest>,
  <variedcommodities:spell_fire>,
  <variedcommodities:ninja_chest>,
  <spartanshields:shield_basic_lumium>,
  <variedcommodities:plans>,
  <variedcommodities:nanorum_legs>,
  <grapplemod:ropeupgradeitem>,
  <variedcommodities:cleaver>,
  <variedcommodities:demonic_sword>,
  <spartanweaponry:greatsword_electrum>,
  <variedcommodities:iron_staff>,
  <spartanweaponry:pike_copper>,
  <spartanweaponry:crossbow_invar>,
  <grapplemod:magnetupgradeitem>,
  <variedcommodities:handcuffs>,
  <spartanweaponry:throwing_knife_tin>,
  <mujmajnkraftsbettersurvival:itemlumiumnunchaku>,
  <variedcommodities:hammer>,
  <variedcommodities:wizard_chest>,
  <spartanweaponry:glaive_invar>,
  <variedcommodities:golden_glaive>,
  <mujmajnkraftsbettersurvival:iteminvarbattleaxe>,
  <spartanweaponry:pike_electrum>,
  <spartanweaponry:spear_copper>,
  <spartanweaponry:glaive_platinum>,
  <variedcommodities:iron_scythe>,
  <variedcommodities:crystal>,
  <spartanweaponry:greatsword_nickel>,
  <variedcommodities:ring>,
  <mujmajnkraftsbettersurvival:itemelectrumbattleaxe>,
  <variedcommodities:frost_broadsword>,
  <variedcommodities:assassin_legs>,
  <spartanshields:shield_basic_nickel>,
  <mujmajnkraftsbettersurvival:itemaluminiumdagger>,
  <spartanweaponry:dagger_lead>,
  <variedcommodities:mana>,
  <variedcommodities:soldier_legs>,
  <spartanweaponry:saber_tin>,
  <variedcommodities:wooden_halberd>,
  <variedcommodities:emerald_dagger_reversed>,
  <variedcommodities:stone_shield_round>,
  <variedcommodities:placeholder>,
  <variedcommodities:silk>,
  <grapplemod:block_grapple_modifier>,
  <variedcommodities:key>,
  <variedcommodities:stone_halberd>,
  <variedcommodities:full_iron_head>,
  <spartanshields:shield_botania_manasteel>,
  <spartanshields:shield_basic_signalum>,
  <variedcommodities:emerald_shield_round>,
  <spartanshields:shield_tower_electrum>,
  <variedcommodities:kunai_reversed>,
  <spartanweaponry:mace_tin>,
  <variedcommodities:tablet>,
  <grapplemod:staffupgradeitem>,
  <variedcommodities:tuxedo_pants>,
  <variedcommodities:mithril_chest>,
  <variedcommodities:diamond_shield>,
  <spartanweaponry:staff_steel>,
  <spartanshields:shield_basic_electrum>,
  <spartanweaponry:battleaxe_lead>,
  <spartanweaponry:halberd_electrum>,
  <variedcommodities:diamond_scythe>,
  <spartanweaponry:longbow_electrum>,
  <variedcommodities:emerald_battleaxe>,
  <spartanweaponry:staff_lead>,
  <variedcommodities:kukri>,
  <variedcommodities:frost_dagger_reversed>,
  <spartanweaponry:javelin_tin>,
  <variedcommodities:letter>,
  <variedcommodities:diamond_gun>,
  <variedcommodities:sledge_hammer>,
  <spartanshields:shield_tower_platinum>,
  <spartanweaponry:saber_electrum>,
  <variedcommodities:chicken_sword>,
  <spartanshields:shield_tower_invar>,
  <variedcommodities:golden_gun>,
  <spartanweaponry:warhammer_nickel>,
  <variedcommodities:soldier_bottom>,
  <variedcommodities:ingot_steel>,
  <variedcommodities:elemental_staff>,
  <spartanweaponry:throwing_knife_lead>,
  <variedcommodities:diamond_trident>,
  <variedcommodities:wooden_shield>,
  <variedcommodities:wooden_scythe>,
  <spartanweaponry:lance_steel>,
  <spartanweaponry:spear_platinum>,
  <spartanweaponry:pike_nickel>,
  <variedcommodities:mithril_sword>,
  <spartanweaponry:hammer_invar>,
  <spartanweaponry:lance_nickel>,
  <variedcommodities:full_diamond_head>,
  <variedcommodities:bronze_sword>,
  <mujmajnkraftsbettersurvival:iteminvarhammer>,
  <spartanshields:shield_tower_lead>,
  <variedcommodities:stone_mace>,
  <variedcommodities:golden_staff>,
  <variedcommodities:stone_shield>,
  <variedcommodities:crossbow>,
  <spartanweaponry:warhammer_steel>,
  <variedcommodities:key2>,
  <variedcommodities:violin_bow>,
  <variedcommodities:wooden_glaive>,
  <spartanshields:shield_abyssalcraft_ethaxium>,
  <variedcommodities:frost_mace>,
  <variedcommodities:bronze_dagger>,
  <variedcommodities:mithril_skirt>,
  <variedcommodities:demonic_dagger_reversed>,
  <variedcommodities:ingot_mithril>,
  <variedcommodities:machine_gun>,
  <variedcommodities:cow_leather_boots>,
  <spartanweaponry:rapier_electrum>,
  <variedcommodities:emerald_glaive>,
  <variedcommodities:lead_pipe>,
  <variedcommodities:iron_shield_round>,
  <variedcommodities:assassin_chest>,
  <variedcommodities:emerald_warhammer>,
  <mujmajnkraftsbettersurvival:itemlumiumspear>,
  <variedcommodities:guitar>,
  <spartanweaponry:throwing_axe_lead>,
  <variedcommodities:full_bronze_legs>,
  <spartanweaponry:staff_platinum>,
  <spartanweaponry:greatsword_platinum>,
  <mujmajnkraftsbettersurvival:itemaluminiumhammer>,
  <variedcommodities:ocarina>,
  <grapplemod:repeller>,
  <variedcommodities:artifact>,
  <spartanweaponry:boomerang_electrum>,
  <variedcommodities:frost_spear>,
  <variedcommodities:stone_glaive>,
  <variedcommodities:gem_ruby>,
  <disenchanter:disenchantmenttable:1>,
  <spartanweaponry:lance_invar>,
  <disenchanter:disenchantmenttable:2>,
  <disenchanter:disenchantmenttable:3>,
  <variedcommodities:coin_iron>,
  <disenchanter:disenchantmenttable:4>,
  <disenchanter:disenchantmenttable:5>,
  <disenchanter:disenchantmenttable:6>,
  <spartanweaponry:warhammer_lead>,
  <variedcommodities:mithril_shield_round>,
  <variedcommodities:demonic_dagger>,
  <variedcommodities:coin_stone>,
  <variedcommodities:emerald_scythe>,
  <mujmajnkraftsbettersurvival:itemelectrumspear>,
  <spartanweaponry:longbow_steel>,
  <spartanshields:shield_basic_lead>,
  <spartanweaponry:javelin_nickel>,
  <variedcommodities:emerald_shield>,
  <mujmajnkraftsbettersurvival:itemsteelnunchaku>,
  <variedcommodities:infantry_helmet>,
  <variedcommodities:excalibur>,
  <spartanweaponry:dagger_invar>,
  <variedcommodities:frost_trident>,
  <spartanweaponry:staff_copper>,
  <variedcommodities:spell_ice>,
  <spartanweaponry:longbow_tin>,
  <variedcommodities:element_fire>,
  <variedcommodities:wrench>,
  <spartanweaponry:hammer_copper>,
  <variedcommodities:full_golden_chest>,
  <variedcommodities:ninja_legs>,
  <variedcommodities:demonic_trident>,
  <variedcommodities:phone>,
  <spartanweaponry:mace_invar>,
  <variedcommodities:frost_halberd>,
  <variedcommodities:full_leather_chest>,
  <variedcommodities:wooden_broadsword>,
  <spartanweaponry:longsword_tin>,
  <variedcommodities:x407_legs>,
  <tconstruct:tooltables:0>,
  <variedcommodities:bronze_warhammer>,
  <spartanweaponry:crossbow_platinum>,
  <spartanweaponry:warhammer_copper>,
  <spartanshields:shield_basic_enderium>,
  <variedcommodities:tuxedo_chest>,
  <mujmajnkraftsbettersurvival:itemsignalumbattleaxe>,
  <variedcommodities:coin_emerald>,
  <spartanweaponry:boomerang_nickel>,
  <variedcommodities:diamond_spear>,
  <variedcommodities:mithril_head>,
  <spartanweaponry:longbow_platinum>,
  <variedcommodities:mithril_warhammer>,
  <variedcommodities:stone_scythe>,
  <variedcommodities:element_earth>,
  <variedcommodities:steel_claw>,
  <variedcommodities:mithril_dagger>,
  <variedcommodities:wooden_dagger_reversed>,
  <spartanweaponry:dagger_nickel>,
  <spartanweaponry:javelin_invar>,
  <variedcommodities:demonic_halberd>,
  <spartanweaponry:battleaxe_invar>,
  <variedcommodities:demonic_battleaxe>,
  <variedcommodities:demonic_boots>,
  <spartanweaponry:longsword_invar>,
  <variedcommodities:golden_dagger>,
  <variedcommodities:frost_scythe>,
  <mujmajnkraftsbettersurvival:itemelectrumnunchaku>,
  <variedcommodities:commissar_legs>,
  <variedcommodities:nanorum_head>,
  <spartanweaponry:throwing_axe_invar>,
  <variedcommodities:emerald_mace>,
  <spartanweaponry:longsword_lead>,
  <spartanweaponry:warhammer_tin>,
  <spartanshields:shield_tc_void>,
  <variedcommodities:element_water>,
  <variedcommodities:full_diamond_chest>,
  <disenchanter:disenchantmenttable:7>,
  <variedcommodities:spell_holy>,
  <spartanweaponry:mace_lead>,
  <spartanweaponry:longsword_copper>,
  <variedcommodities:magic_wand>,
  <variedcommodities:musket>,
  <spartanweaponry:javelin_lead>,
  <variedcommodities:demonic_broadsword>,
  <spartanweaponry:boomerang_lead>,
  <variedcommodities:mithril_staff>,
  <grapplemod:swingupgradeitem>,
  <variedcommodities:rapier>,
  <variedcommodities:tower_shield>,
  <variedcommodities:diamond_warhammer>,
  <variedcommodities:diamond_shield_round>,
  <variedcommodities:demonic_mace>,
  <variedcommodities:lighter>,
  <variedcommodities:car_key>,
  <variedcommodities:mithril_trident>,
  <spartanweaponry:hammer_lead>,
  <variedcommodities:blueprint>,
  <variedcommodities:coin_gold>,
  <variedcommodities:swiss_army_knife>,
  <spartanweaponry:lance_lead>,
  <spartanweaponry:staff_nickel>,
  <spartanweaponry:hammer_nickel>,
  <variedcommodities:orb_broken>,
  <variedcommodities:scutum_shield>,
  <variedcommodities:blood_block>,
  <spartanshields:shield_basic_constantan>,
  <variedcommodities:spell_nature>,
  <spartanshields:shield_tc_thaumium>,
  <variedcommodities:banjo>,
  <spartanweaponry:glaive_lead>,
  <spartanweaponry:spear_lead>,
  <mujmajnkraftsbettersurvival:itemsignalumnunchaku>,
  <variedcommodities:emerald_sword>,
  <spartanweaponry:throwing_knife_nickel>,
  <variedcommodities:stone_staff>,
  <variedcommodities:commissar_chest>,
  <spartanweaponry:pike_invar>,
  <spartanweaponry:saber_copper>,
  <variedcommodities:frost_warhammer>,
  <mujmajnkraftsbettersurvival:iteminvarspear>,
  <variedcommodities:bronze_trident>,
  <spartanshields:shield_tower_nickel>,
  <spartanshields:shield_botania_elementium>,
  <variedcommodities:shuriken_giant>,
  <spartanweaponry:katana_platinum>,
  <variedcommodities:hockey_stick>,
  <spartanweaponry:rapier_tin>,
  <variedcommodities:satchel>,
  <variedcommodities:bag>,
  <variedcommodities:golf_club>,
  <mujmajnkraftsbettersurvival:itemaluminiumnunchaku>,
  <variedcommodities:emerald_trident>,
  <spartanweaponry:longbow_nickel>,
  <variedcommodities:money>,
  <spartanweaponry:throwing_axe_platinum>,
  <spartanweaponry:crossbow_electrum>,
  <variedcommodities:full_emerald_head>,
  <variedcommodities:frost_dagger>,
  <mujmajnkraftsbettersurvival:itemenderiumbattleaxe>,
  <variedcommodities:sai>,
  <variedcommodities:frost_glaive>,
  <spartanweaponry:dagger_electrum>,
  <variedcommodities:bandit_mask>,
  <spartanweaponry:throwing_knife_electrum>,
  <spartanweaponry:boomerang_invar>,
  <spartanweaponry:battleaxe_electrum>,
  <variedcommodities:severed_ear>,
  <spartanshields:shield_abyssalcraft_abyssalnite>,
  <grapplemod:motorupgradeitem>,
  <variedcommodities:iron_dagger_reversed>,
  <spartanweaponry:longbow_copper>,
  <variedcommodities:demonic_warhammer>,
  <variedcommodities:katana>,
  <variedcommodities:full_wooden_chest>,
  <spartanweaponry:katana_lead>,
  <variedcommodities:combat_knive>,
  <variedcommodities:bronze_halberd>,
  <spartanweaponry:rapier_invar>,
  <variedcommodities:pipe_wrench>,
  <variedcommodities:soldier_head>,
  <spartanweaponry:longsword_platinum>,
  <spartanshields:shield_tower_constantan>,
  <variedcommodities:golden_broadsword>,
  <variedcommodities:full_wooden_legs>,
  <variedcommodities:iron_gun>,
  <spartanweaponry:rapier_platinum>,
  <variedcommodities:harp>,
  <spartanweaponry:throwing_axe_nickel>,
  <spartanshields:shield_tower_copper>,
  <variedcommodities:emerald_broadsword>,
  <variedcommodities:tactical_chest>,
  <variedcommodities:cow_leather_legs>,
  <variedcommodities:x407_chest>,
  <spartanweaponry:spear_steel>,
  <variedcommodities:iron_battleaxe>,
  <variedcommodities:demonic_spear>,
  <spartanweaponry:throwing_knife_copper>,
  <spartanweaponry:rapier_lead>,
  <spartanweaponry:staff_invar>,
  <spartanweaponry:warhammer_invar>,
  <variedcommodities:emerald_halberd>,
  <variedcommodities:diamond_broadsword>,
  <spartanshields:shield_flux_ra>,
  <variedcommodities:nanorum_boots>,
  <mujmajnkraftsbettersurvival:itemenderiumdagger>,
  <variedcommodities:usb_stick>,
  <mujmajnkraftsbettersurvival:itemelectrumdagger>,
  <grapplemod:forcefieldupgradeitem>,
  <variedcommodities:bronze_broadsword>,
  <spartanweaponry:crossbow_nickel>,
  <spartanweaponry:halberd_copper>,
  <spartanweaponry:javelin_platinum>,
  <mujmajnkraftsbettersurvival:itemsignalumhammer>,
  <variedcommodities:full_emerald_boots>,
  <notreepunching:fire_starter>,
  <spartanweaponry:throwing_axe_copper>,
  <variedcommodities:gem_amethyst>,
  <variedcommodities:stone_gun>,
  <mujmajnkraftsbettersurvival:itemenderiumspear>,
  <spartanweaponry:pike_tin>,
  <variedcommodities:golden_mace>,
  <variedcommodities:emerald_staff>,
  <spartanweaponry:throwing_axe_tin>,
  <spartanweaponry:warhammer_electrum>,
  <variedcommodities:mithril_dagger_reversed>,
  <variedcommodities:stone_warhammer>,
  <variedcommodities:crown>,
  <variedcommodities:ingot_demonic>,
  <spartanshields:shield_basic_invar>,
  <variedcommodities:officer_chest>,
  <spartanweaponry:halberd_tin>,
  <grapplemod:limitsupgradeitem>,
  <variedcommodities:violin>,
  <variedcommodities:skull>,
  <spartanweaponry:greatsword_tin>,
  <variedcommodities:saber>,
  <variedcommodities:frost_battleaxe>,
  <spartanweaponry:saber_lead>,
  <spartanweaponry:saber_steel>,
  <spartanweaponry:javelin_electrum>,
  <variedcommodities:wooden_mace>,
  <variedcommodities:wooden_spear>,
  <spartanweaponry:greatsword_invar>,
  <variedcommodities:stone_dagger_reversed>,
  <variedcommodities:bronze_scythe>,
  <spartanweaponry:hammer_electrum>,
  <variedcommodities:bronze_shield_round>,
  <spartanweaponry:boomerang_copper>,
  <variedcommodities:diamond_staff>,
  <mujmajnkraftsbettersurvival:itemlumiumdagger>,
  <variedcommodities:clarinet>,
  <variedcommodities:golden_battleaxe>,
  <spartanweaponry:pike_lead>,
  <spartanweaponry:dagger_copper>,
  <spartanweaponry:katana_electrum>,
  <switchbow:arrowdiamond>,
  <variedcommodities:iron_halberd>,
  <variedcommodities:emerald_skirt>,
  <variedcommodities:nanorum_chest>,
  <variedcommodities:golden_shield_round>,
  <variedcommodities:assassin_head>,
  <spartanweaponry:halberd_nickel>,
  <spartanweaponry:throwing_knife_steel>,
  <variedcommodities:mithril_spear>,
  <variedcommodities:diamond_mace>,
  <spartanweaponry:battleaxe_platinum>,
  <spartanweaponry:katana_invar>,
  <spartanweaponry:hammer_tin>,
  <variedcommodities:bronze_spear>,
  <variedcommodities:full_bronze_boots>,
  <mujmajnkraftsbettersurvival:itemaluminiumspear>,
  <variedcommodities:diamond_battleaxe>,
  <spartanweaponry:staff_tin>,
  <variedcommodities:stone_spear>,
  <variedcommodities:x407_boots>,
  <variedcommodities:wizard_pants>,
  <variedcommodities:mithril_mace>,
  <variedcommodities:ancient_coin>,
  <spartanweaponry:dagger_steel>,
  <variedcommodities:diamond_dagger>,
  <variedcommodities:heart>,
  <spartanweaponry:warhammer_platinum>,
  <variedcommodities:mithril_halberd>,
  <mujmajnkraftsbettersurvival:iteminvardagger>,
  <variedcommodities:iron_dagger>,
  <spartanweaponry:halberd_lead>,
  <spartanweaponry:crossbow_tin>,
  <spartanweaponry:longbow_lead>,
  <variedcommodities:demonic_legs>,
  <spartanweaponry:javelin_copper>,
  <mujmajnkraftsbettersurvival:itemcopperbattleaxe>,
  <variedcommodities:chainsaw_gun>,
  # Custom
  # slimesling and slime boots
  <tconstruct:slimesling>,
  <tconstruct:slime_boots>,
  # mw Armor
  <mw:mkvi_main_chest>,
  <mw:mkv_green_chest>,
  <mw:mkv_default_odst_helmet>,
  <mw:mkv_default_odst_chest>,
  <mw:mkvi_main_boots>,
  <mw:mkviblue_chest>,
  <mw:mkv_blue_boots>,
  <mw:mkv_blue_odst_helmet>,
  <mw:mkv_red_odst_boots>,
  <mw:mkv_green_boots>,
  <mw:mkv_red_chest>,
  <mw:mkvidefault_boots>,
  <mw:mkv_blue_odst_boots>,
  <mw:mkviblue_boots>,
  <mw:mkv_blue_helmet>,
  <mw:mkv_blue_odst_chest>,
  <mw:mkvidefault_chest>,
  <mw:mkv_red_boots>,
  <mw:mkviblue_helmet>,
  <mw:mkv_default_helmet>,
  <mw:mkv_green_odst_chest>,
  <mw:mkv_default_boots>,
  <mw:mkvi_main_helmet>,
  <mw:mkv_red_helmet>,
  <mw:mkv_green_odst_boots>,
  <mw:mkvired_boots>,
  <mw:mkvired_helmet>,
  <mw:mkv_default_odst_boots>,
  <mw:mkv_red_odst_helmet>,
  <mw:mkv_green_odst_helmet>,
  <mw:mkv_blue_chest>,
  <mw:mkv_default_chest>,
  <mw:mkv_green_helmet>,
  <mw:mkv_red_odst_chest>,
  <mw:mkvired_chest>,
  <mw:mkvidefault_helmet>,
  # angelring
  <extrautils2:angelring>,
  # ic2 jetpack
  <ic2:jetpack_electric>,
  <ic2:jetpack>,
  # mw Weapons
  <mw:m16a4>,
  <mw:mk14_ebr>,
  <mw:m249>,
  <mw:csa16>,
  <mw:bfgammo>,
  <mw:gpca1>,
  <mw:mp5a5>,
  <mw:saiga12>,
  <mw:sl8>,
  <mw:esa23>,
  <mw:har_27>,
  <mw:m82_barrett>,
  <mw:m8a7>,
  <mw:stonera1>,
  <mw:ak47>,
  <mw:mininuke>,
  <mw:kbp_9a91>,
  <mw:scar_h_cqc>,
  <mw:mp7>,
  <mw:m1928_thompson>,
  <mw:vss_vintorez>,
  <mw:g36c>,
  <mw:type2>,
  <mw:mg42>,
  <mw:m32_mgl>,
  <mw:izhmash_ak12>,
  <mw:supernova>,
  <mw:m4a1>,
  <mw:p90>,
  <mw:ma37_icws>,
  <mw:m110_sass>,
  <mw:scar_l>,
  <mw:chiappa_triple_crown>,
  <mw:srs99>,
  <mw:m20>,
  <mw:m392>,
  <mw:nv4>,
  <mw:br55>,
  <mw:mg34>,
  <mw:m41a>,
  <mw:uzi>,
  <mw:m60e4>,
  <mw:browning_auto_5>,
  <mw:sr3_vikhr>,
  <mw:f2000>,
  <mw:volk>,
  <mw:ma5d>,
  <mw:bfg_9000>,
  <mw:m56>,
  <mw:aac_honey_badger>,
  <mw:dp28>,
  <mw:ntw_20>,
  <mw:g11>,
  <mw:steyr_aug_a1>,
  <mw:ak101>,
  <mw:mp43e>,
  <mw:m1941_johnson>,
  <mw:fatman>,
  <mw:m38_dmr>,
  <mw:mares_leg>,
  <mw:acr>,
  <mw:ak15>,
  <mw:desert_eagle>,
  <mw:kriss_vector>,
  <mw:as50>,
  <mw:m1014>,
  <mw:quad_barrel_shotgun>,
  <mw:sv98>,
  <mw:type51>,
  <mw:m134>,
  <mw:spas_12>,
  <mw:bren_mkii>,
  <mw:plasmacapsule>,
  <mw:m45>,
  <mw:m1_garand>,
  <mw:ak103_bullpup>,
  <mw:m40a6>,
  <mw:cz805_bren>,
  <mw:m1941_johnson_rifle>
];

for item in itemArray {
  JEI.removeAndHide(item);
}

recipes.remove(<qualitytools:emerald_ring>);
recipes.remove(<qualitytools:emerald_amulet>);
recipes.remove(<antiqueatlas:empty_antique_atlas>);
recipes.remove(<waystones:waystone>);
recipes.remove(<waystones:warp_stone>);
recipes.remove(<firstaid:plaster>);
recipes.remove(<firstaid:bandage>);
recipes.remove(<roughtweaks:salve>);
recipes.remove(<roughtweaks:plaster>);
recipes.remove(<roughtweaks:bandage>);

recipes.remove(<xat:weightless_stone>);
recipes.remove(<xat:fairy_ring>);
recipes.remove(<xat:dwarf_ring>);
recipes.remove(<xat:inertia_null_stone>);
recipes.remove(<xat:greater_inertia_stone>);
recipes.remove(<xat:damage_shield>);

recipes.removeByRecipeName("bountifulbaubles:spectralsilt_flaregun");
recipes.removeByRecipeName("bountifulbaubles:flare_red");
recipes.removeByRecipeName("bountifulbaubles:phantomprism");
recipes.removeByRecipeName("bountifulbaubles:spectralsilt_magicmirror");
recipes.removeByRecipeName("bountifulbaubles:wormholemirror");
recipes.removeByRecipeName("bountifulbaubles:spectralsilt_luckyhorseshoe");
recipes.removeByRecipeName("bountifulbaubles:ringflywheel");
recipes.removeByRecipeName("bountifulbaubles:ringflywheeladvanced");
recipes.removeByRecipeName("bountifulbaubles:ringiron");
recipes.removeByRecipeName("bountifulbaubles:spectralsilt_sinpendantempty");

recipes.remove(<campfire:campfire>);

recipes.remove(<potionfingers:ring:1>);

recipes.remove(<xat:glowing_ingot>);

recipes.remove(<minecraft:snow_layer>);

recipes.remove(<switchbow:arrowdiamond>);

recipes.remove(<minecraft:web>);
recipes.remove(<sereneseasons:greenhouse_glass>);

recipes.remove(<mujmajnkraftsbettersurvival:itemwooddagger>);
recipes.remove(<mujmajnkraftsbettersurvival:itemstonedagger>);
recipes.remove(<mujmajnkraftsbettersurvival:itemirondagger>);
recipes.remove(<mujmajnkraftsbettersurvival:itemgolddagger>);
recipes.remove(<mujmajnkraftsbettersurvival:itemdiamonddagger>);
recipes.remove(<mujmajnkraftsbettersurvival:itemsilverdagger>);
recipes.remove(<mujmajnkraftsbettersurvival:itembronzedagger>);

recipes.removeByRecipeName("notreepunching:tools/fire_starter");
recipes.remove(<minecraft:sandstone>);
recipes.remove(<minecraft:red_sandstone>);

furnace.remove(<notreepunching:ceramic_small_vessel>);

furnace.remove(<minecraft:gold_nugget>);
furnace.remove(<minecraft:iron_nugget>);
furnace.remove(<defiledlands:umbrium_nugget>);
furnace.remove(<variedcommodities:ingot_steel>);

furnace.remove(<iceandfire:silver_ingot>);

recipes.remove(<wolfarmor:chainmail_wolf_armor>);

recipes.remove(<firstaid:bandage>);

furnace.addRecipe(<minecraft:diamond> * 2, <minecraft:diamond_helmet:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 4, <minecraft:diamond_chestplate:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 3, <minecraft:diamond_leggings:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <minecraft:diamond_boots:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 2, <minecraft:iron_helmet:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 4, <minecraft:iron_chestplate:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 3, <minecraft:iron_leggings:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <minecraft:iron_boots:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 2, <minecraft:golden_helmet:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 4, <minecraft:golden_chestplate:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 3, <minecraft:golden_leggings:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <minecraft:golden_boots:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <minecraft:diamond_sword:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <minecraft:diamond_pickaxe:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <minecraft:diamond_axe:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <minecraft:diamond_hoe:*>, 99999);
furnace.addRecipe(<variedcommodities:coin_diamond> * 4, <minecraft:diamond_shovel:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <minecraft:iron_sword:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <minecraft:iron_pickaxe:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <minecraft:iron_axe:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <minecraft:iron_hoe:*>, 99999);
furnace.addRecipe(<minecraft:iron_nugget> * 4, <minecraft:iron_shovel:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <minecraft:golden_sword:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <minecraft:golden_pickaxe:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <minecraft:golden_axe:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <minecraft:golden_hoe:*>, 99999);
furnace.addRecipe(<minecraft:gold_nugget> * 4, <minecraft:golden_shovel:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 8, <minecraft:diamond_horse_armor:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 8, <minecraft:iron_horse_armor:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 8, <minecraft:golden_horse_armor:*>, 99999);
furnace.addRecipe(<minecraft:iron_nugget> * 11, <minecraft:chainmail_helmet:*>, 99999);
furnace.addRecipe(<minecraft:iron_nugget> * 18, <minecraft:chainmail_chestplate:*>, 99999);
furnace.addRecipe(<minecraft:iron_nugget> * 15, <minecraft:chainmail_leggings:*>, 99999);
furnace.addRecipe(<minecraft:iron_nugget> * 9, <minecraft:chainmail_boots:*>, 99999);

furnace.addRecipe(<minecraft:diamond> * 3, <mujmajnkraftsbettersurvival:itemdiamondhammer:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 2, <mujmajnkraftsbettersurvival:itemdiamondbattleaxe:*>, 99999);
furnace.addRecipe(<variedcommodities:coin_diamond> * 4, <mujmajnkraftsbettersurvival:itemdiamonddagger:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <mujmajnkraftsbettersurvival:itemdiamondnunchaku:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 3, <mujmajnkraftsbettersurvival:itemironhammer:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 2, <mujmajnkraftsbettersurvival:itemironbattleaxe:*>, 99999);
furnace.addRecipe(<minecraft:iron_nugget> * 4, <mujmajnkraftsbettersurvival:itemirondagger:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <mujmajnkraftsbettersurvival:itemironnunchaku:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 3, <mujmajnkraftsbettersurvival:itemgoldhammer:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 2, <mujmajnkraftsbettersurvival:itemgoldbattleaxe:*>, 99999);
furnace.addRecipe(<minecraft:gold_nugget> * 4, <mujmajnkraftsbettersurvival:itemgolddagger:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <mujmajnkraftsbettersurvival:itemgoldnunchaku:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 3, <mujmajnkraftsbettersurvival:itemsilverhammer:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 2, <mujmajnkraftsbettersurvival:itemsilverbattleaxe:*>, 99999);
furnace.addRecipe(<iceandfire:silver_nugget> * 4, <mujmajnkraftsbettersurvival:itemsilverdagger:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <mujmajnkraftsbettersurvival:itemsilvernunchaku:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 3, <mujmajnkraftsbettersurvival:itembronzehammer:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 2, <mujmajnkraftsbettersurvival:itembronzebattleaxe:*>, 99999);
furnace.addRecipe(<variedcommodities:coin_bronze> * 4, <mujmajnkraftsbettersurvival:itembronzedagger:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <mujmajnkraftsbettersurvival:itembronzenunchaku:*>, 99999);

furnace.addRecipe(<minecraft:diamond> * 2, <spartanweaponry:longsword_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:katana_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:saber_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 2, <spartanweaponry:rapier_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 3, <spartanweaponry:greatsword_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 3, <spartanweaponry:hammer_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:warhammer_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 2, <spartanweaponry:halberd_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:throwing_axe_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 2, <spartanweaponry:battleaxe_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:mace_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:glaive_diamond:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 1, <spartanweaponry:staff_diamond:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 2, <spartanweaponry:longsword_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:katana_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:saber_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 2, <spartanweaponry:rapier_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 3, <spartanweaponry:greatsword_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 3, <spartanweaponry:hammer_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:warhammer_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 2, <spartanweaponry:halberd_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:throwing_axe_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 2, <spartanweaponry:battleaxe_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:mace_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:glaive_iron:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 1, <spartanweaponry:staff_iron:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 2, <spartanweaponry:longsword_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:katana_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:saber_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 2, <spartanweaponry:rapier_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 3, <spartanweaponry:greatsword_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 3, <spartanweaponry:hammer_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:warhammer_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 2, <spartanweaponry:halberd_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:throwing_axe_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 2, <spartanweaponry:battleaxe_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:mace_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:glaive_gold:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 1, <spartanweaponry:staff_gold:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 2, <spartanweaponry:longsword_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:katana_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:saber_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 2, <spartanweaponry:rapier_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 3, <spartanweaponry:greatsword_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 3, <spartanweaponry:hammer_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:warhammer_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 2, <spartanweaponry:halberd_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:throwing_axe_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 2, <spartanweaponry:battleaxe_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:mace_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:glaive_silver:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <spartanweaponry:staff_silver:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 2, <spartanweaponry:longsword_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:katana_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:saber_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 2, <spartanweaponry:rapier_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 3, <spartanweaponry:greatsword_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 3, <spartanweaponry:hammer_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:warhammer_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 2, <spartanweaponry:halberd_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:throwing_axe_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 2, <spartanweaponry:battleaxe_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:mace_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:glaive_bronze:*>, 99999);
furnace.addRecipe(<variedcommodities:ingot_bronze> * 1, <spartanweaponry:staff_bronze:*>, 99999);

furnace.addRecipe(<defiledlands:umbrium_ingot> * 1, <defiledlands:umbrium_sword:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 1, <defiledlands:umbrium_axe:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 1, <defiledlands:umbrium_pickaxe:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 1, <defiledlands:umbrium_hoe:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_nugget> * 4, <defiledlands:umbrium_shovel:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 2, <defiledlands:umbrium_helmet:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 4, <defiledlands:umbrium_chestplate:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 3, <defiledlands:umbrium_leggings:*>, 99999);
furnace.addRecipe(<defiledlands:umbrium_ingot> * 2, <defiledlands:umbrium_boots:*>, 99999);

furnace.addRecipe(<iceandfire:silver_ingot> * 2, <iceandfire:armor_silver_metal_helmet:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 4, <iceandfire:armor_silver_metal_chestplate:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 3, <iceandfire:armor_silver_metal_leggings:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 2, <iceandfire:armor_silver_metal_boots:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <iceandfire:silver_sword:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <iceandfire:silver_axe:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <iceandfire:silver_pickaxe:*>, 99999);
furnace.addRecipe(<iceandfire:silver_ingot> * 1, <iceandfire:silver_hoe:*>, 99999);
furnace.addRecipe(<iceandfire:silver_nugget> * 4, <iceandfire:silver_shovel:*>, 99999);

furnace.addRecipe(<minecraft:iron_nugget> * 30, <wolfarmor:chainmail_wolf_armor:*>, 99999);
furnace.addRecipe(<minecraft:iron_ingot> * 7, <wolfarmor:iron_wolf_armor:*>, 99999);
furnace.addRecipe(<minecraft:gold_ingot> * 7, <wolfarmor:gold_wolf_armor:*>, 99999);
furnace.addRecipe(<minecraft:diamond> * 7, <wolfarmor:diamond_wolf_armor:*>, 99999);

furnace.addRecipe(<iceandfire:silver_ingot> * 1, <iceandfire:silver_ore:*>, 99999);
furnace.addRecipe(<iceandfire:sapphire_gem> * 1, <iceandfire:sapphire_ore:*>, 99999);

brewing.addBrew(<minecraft:potion>.withTag({Potion: "minecraft:awkward"}), <waystones:warp_scroll>, <bountifulbaubles:potionrecall>);
brewing.addBrew(<bountifulbaubles:potionrecall>, <minecraft:ender_eye>, <bountifulbaubles:potionwormhole>);

recipes.addShapeless("lolarecipe1",<antiqueatlas:empty_antique_atlas>,[<minecraft:writable_book>,<minecraft:compass>]);

recipes.addShapeless("lolarecipe2",<roughtweaks:salve>,[<minecraft:bowl>,<minecraft:red_flower:*>,<minecraft:yellow_flower>]);

recipes.addShapeless("lolarecipe3",<roughtweaks:salve>,[<minecraft:bowl>,<minecraft:cactus>]);

recipes.addShapeless("lolarecipe4",<roughtweaks:salve>,[<minecraft:bowl>,<minecraft:wheat_seeds>,<minecraft:vine>]);

recipes.addShapeless("lolarecipe5",<roughtweaks:plaster>,[<roughtweaks:salve>,<ore:string>,<minecraft:paper>]);

recipes.addShaped("lolarecipe6",<roughtweaks:bandage>,
 [[null,null,null],
  [<ore:string>,<minecraft:wool:*>,<ore:string>],
  [null,<roughtweaks:salve>,null]]);

recipes.addShaped("lolarecipe7",<minecraft:golden_apple:1>,
 [[<minecraft:gold_block>,<minecraft:gold_block>,<minecraft:gold_block>],
  [<minecraft:gold_block>,<minecraft:apple>,<minecraft:gold_block>],
  [<minecraft:gold_block>,<minecraft:gold_block>,<minecraft:gold_block>]]);

recipes.addShapeless("lolarecipe8",<minecraft:string>*4,[<minecraft:wool:*>]);

recipes.addShaped("lolarecipe9",<minecraft:packed_ice>,
 [[<toughasnails:ice_cube>,<toughasnails:ice_cube>,<toughasnails:ice_cube>],
  [<toughasnails:ice_cube>,<toughasnails:ice_cube>,<toughasnails:ice_cube>],
  [<toughasnails:ice_cube>,<toughasnails:ice_cube>,<toughasnails:ice_cube>]]);

recipes.addShapeless("lolarecipe10",<firstaid:plaster>,[<ore:string>,<minecraft:paper>]);

recipes.addShaped("lolarecipe11",<firstaid:bandage>*2,
 [[<ore:string>,<minecraft:wool:*>,<ore:string>]]);

recipes.addShaped("lolarecipe12",<mujmajnkraftsbettersurvival:itemwooddagger>,
 [[null,<minecraft:planks:*>],
  [<minecraft:stick>]]);

recipes.addShaped("lolarecipe13",<mujmajnkraftsbettersurvival:itemstonedagger>,
 [[null,<ore:cobblestone>],
  [<minecraft:stick>]]);

recipes.addShaped("lolarecipe14",<mujmajnkraftsbettersurvival:itemgolddagger>,
 [[null,<minecraft:gold_ingot>],
  [<minecraft:stick>]]);

recipes.addShaped("lolarecipe15",<mujmajnkraftsbettersurvival:itemirondagger>,
 [[null,<minecraft:iron_ingot>],
  [<minecraft:stick>]]);

recipes.addShaped("lolarecipe16",<mujmajnkraftsbettersurvival:itemdiamonddagger>,
 [[null,<minecraft:diamond>],
  [<minecraft:stick>]]);
  
recipes.addShaped("lolarecipe16again",<mujmajnkraftsbettersurvival:itemsilverdagger>,
 [[null,<iceandfire:silver_ingot>],
  [<minecraft:stick>]]);
  
recipes.addShaped("lolarecipe16againagain",<mujmajnkraftsbettersurvival:itembronzedagger>,
 [[null,<variedcommodities:ingot_bronze>],
  [<minecraft:stick>]]);

recipes.addShaped("lolarecipe17",<mujmajnkraftsbettersurvival:itemstonespear>,
 [[null,null,<ore:cobblestone>],
  [null,<minecraft:stick>,null],
  [<minecraft:stick>,null,null]]);

recipes.addShaped("lolarecipe18",<mujmajnkraftsbettersurvival:itemstonebattleaxe>,
 [[null,<ore:cobblestone>,<ore:cobblestone>],
  [<minecraft:stick>,<minecraft:stick>,<ore:cobblestone>],
  [null,<ore:cobblestone>,<ore:cobblestone>]]);
  
recipes.addShaped("lolarecipe19",<mujmajnkraftsbettersurvival:itemstonenunchaku>,
 [[null,null,null],
  [null,<ore:string>,null],
  [<ore:cobblestone>,null,<ore:cobblestone>]]);
  
recipes.addShaped("lolarecipe20",<mujmajnkraftsbettersurvival:itemstonehammer>,
 [[null,<ore:cobblestone>,<ore:cobblestone>],
  [<minecraft:stick>,<ore:cobblestone>,<ore:cobblestone>],
  [null,<ore:cobblestone>,<ore:cobblestone>]]);

//recipes.addShapeless("lolarecipe21",<minecraft:paper>*3,[<minecraft:log:2>]);

recipes.addShapeless("lolarecipe21test",<minecraft:paper>*4,[<minecraft:log:2>,<minecraft:log:2>]);

recipes.addShaped("lolarecipe22",<qualitytools:emerald_ring>,
 [[null,<minecraft:emerald>,null],
  [<minecraft:gold_ingot>,null,<minecraft:gold_ingot>],
  [null,<minecraft:gold_ingot>,null]]);
  
recipes.addShaped("lolarecipe23",<qualitytools:emerald_amulet>,
 [[<minecraft:gold_ingot>,<minecraft:gold_ingot>,<minecraft:gold_ingot>],
  [<minecraft:gold_ingot>,null,<minecraft:gold_ingot>],
  [null,<minecraft:emerald>,null]]);
  
recipes.addShaped("lolarecipe24",<xat:dragons_eye>,
 [[<xat:glow_ring>,<minecraft:diamond_sword>,<xat:glowing_gem>],
  [<minecraft:diamond_sword>,<iceandfire:dragon_skull>,<minecraft:diamond_sword>],
  [<xat:glowing_gem>,<minecraft:diamond_sword>,<xat:glowing_gem>]]);
  
recipes.addShaped("lolarecipe25",<xat:wither_ring>,
 [[<xat:glowing_ingot>,<minecraft:iron_ingot>,<xat:glowing_ingot>],
  [<minecraft:iron_ingot>,<minecraft:skull:1>|<iceandfire:dragon_skull:1>,<minecraft:iron_ingot>],
  [<xat:glowing_ingot>,<minecraft:iron_ingot>,<xat:glowing_ingot>]]);
  
recipes.addShaped("lolarecipe26",<campfire:campfire>,
 [[null,<realistictorches:matchbox>.anyDamage().transformDamage(),null],
  [null,<ore:plankWood>,null],
  [<ore:plankWood>,<ore:plankWood>,<ore:plankWood>]]);
  
recipes.addShaped("lolarecipe27",<bountifulbaubles:crowngold>,
 [[null,<defiledlands:scarlite>,null],
  [<minecraft:gold_ingot>,<minecraft:gold_ingot>,<minecraft:gold_ingot>],
  [<minecraft:gold_ingot>,null,<minecraft:gold_ingot>]]);
  
recipes.addShaped("lolarecipe28",<wolfarmor:chainmail_wolf_armor>,
 [[null,<minecraft:chainmail_helmet>,null],
  [<craftablechainmail:chainmail_plate>,<minecraft:chainmail_boots>,<craftablechainmail:chainmail_plate>],
  [null,<minecraft:chainmail_boots>,null]]);
  
recipes.addShapeless("lolarecipe28wolfleatherrrr",<minecraft:leather>*7,[<wolfarmor:leather_wolf_armor>]);
  
recipes.addShaped("lolarecipe29",<scalinghealth:crystalshard>,
 [[<scalinghealth:heartdust>,<scalinghealth:heartdust>,<scalinghealth:heartdust>],
  [<scalinghealth:heartdust>,<scalinghealth:heartdust>,<scalinghealth:heartdust>],
  [<scalinghealth:heartdust>,<scalinghealth:heartdust>,<scalinghealth:heartdust>]]);

recipes.addShaped("lolarecipe30",<bountifulbaubles:amuletsinempty>,
 [[<bountifulbaubles:spectralsilt>,<ore:string>,<bountifulbaubles:spectralsilt>],
  [<bountifulbaubles:spectralsilt>,<iceandfire:silver_ingot>,<bountifulbaubles:spectralsilt>],
  [<bountifulbaubles:spectralsilt>,<bountifulbaubles:spectralsilt>,<bountifulbaubles:spectralsilt>]]);
  
recipes.addShapeless("lolarecipe31",<bountifulbaubles:spectralsilt>,[<bountifulbaubles:disintegrationtablet>,<minecraft:diamond_block>]);

recipes.addShapeless("lolarecipe32",<bountifulbaubles:spectralsilt>,[<bountifulbaubles:disintegrationtablet>,<minecraft:emerald_block>]);

recipes.addShapeless("lolarecipe31x",<bountifulbaubles:spectralsilt>,[<bountifulbaubles:disintegrationtablet>,<bountifulbaubles:phantomprism>]);

recipes.addShaped("lolarecipe33",<advanced-fishing:blazing_fishing_pole>,
 [[null,null,<minecraft:blaze_rod>],
  [null,<minecraft:blaze_rod>,<ore:string>],
  [<minecraft:blaze_rod>,<minecraft:magma_cream>,<ore:string>]]);
  
recipes.addShapeless("lolarecipe35",<armorunder:auto_chestplate_liner>,[<armorunder:warm_chestplate_liner>,<armorunder:cool_chestplate_liner>]);

recipes.addShapeless("lolarecipe36",<armorunder:auto_leggings_liner>,[<armorunder:warm_leggings_liner>,<armorunder:cool_leggings_liner>]);

recipes.addShaped("lolarecipe37",<minecraft:web>,
 [[<ore:string>,<ore:string>,<ore:string>],
  [<ore:string>,<ore:string>,<ore:string>],
  [<ore:string>,<ore:string>,<ore:string>]]);
  
recipes.addShaped("lolarecipe38",<sereneseasons:greenhouse_glass>*4,
 [[<ore:dyeCyan>,<ore:blockGlass>,<ore:dyeCyan>],
  [<ore:blockGlass>,<ore:plankWood>,<ore:blockGlass>],
  [<ore:dyeCyan>,<ore:blockGlass>,<ore:dyeCyan>]]);
  
recipes.addShapeless("lolarecipe39",<realistictorches:torch_lit>,[<realistictorches:torch_unlit>,<minecraft:flint_and_steel>.anyDamage().transformDamage()]);

recipes.addShapeless("lolarecipe40",<armorunder:flipflop_liner_material>,[<armorunder:warm_liner_material>,<armorunder:cool_liner_material>]);

recipes.addShaped("lolarecipe41",<armorunder:auto_chestplate_liner>,
 [[<armorunder:flipflop_liner_material>,null,<armorunder:flipflop_liner_material>],
  [<armorunder:flipflop_liner_material>,<armorunder:flipflop_liner_material>,<armorunder:flipflop_liner_material>],
  [<armorunder:flipflop_liner_material>,<armorunder:flipflop_liner_material>,<armorunder:flipflop_liner_material>]]);
  
recipes.addShaped("lolarecipe42",<armorunder:auto_leggings_liner>,
 [[<armorunder:flipflop_liner_material>,<armorunder:flipflop_liner_material>,<armorunder:flipflop_liner_material>],
  [<armorunder:flipflop_liner_material>,null,<armorunder:flipflop_liner_material>],
  [<armorunder:flipflop_liner_material>,null,<armorunder:flipflop_liner_material>]]);
  
recipes.addShaped("lolarecipe43",<waystones:waystone>,
 [[null,<minecraft:stonebrick>,null],
  [<minecraft:stonebrick>,<minecraft:nether_star>,<minecraft:stonebrick>],
  [<minecraft:obsidian>,<minecraft:obsidian>,<minecraft:obsidian>]]);
  
recipes.addShaped("lolarecipe44",<minecraft:diamond>,
 [[<variedcommodities:coin_diamond>,<variedcommodities:coin_diamond>,<variedcommodities:coin_diamond>],
  [<variedcommodities:coin_diamond>,<variedcommodities:coin_diamond>,<variedcommodities:coin_diamond>],
  [<variedcommodities:coin_diamond>,<variedcommodities:coin_diamond>,<variedcommodities:coin_diamond>]]);
  
recipes.addShapeless("lolarecipe45",<variedcommodities:coin_diamond> * 9,[<minecraft:diamond>]);

recipes.addShaped("lolarecipe44b",<variedcommodities:ingot_bronze>,
 [[<variedcommodities:coin_bronze>,<variedcommodities:coin_bronze>,<variedcommodities:coin_bronze>],
  [<variedcommodities:coin_bronze>,<variedcommodities:coin_bronze>,<variedcommodities:coin_bronze>],
  [<variedcommodities:coin_bronze>,<variedcommodities:coin_bronze>,<variedcommodities:coin_bronze>]]);
  
recipes.addShapeless("lolarecipe45b",<variedcommodities:coin_bronze> * 9,[<variedcommodities:ingot_bronze>]);

recipes.addShaped("lolarecipe46",<potionfingers:ring:1>.withTag({Quality: {}, effect: "minecraft:speed"}),
 [[<minecraft:redstone_block>, <minecraft:sugar>, null],
  [<minecraft:sugar>, <potionfingers:ring>, <minecraft:sugar>],
  [null, <minecraft:sugar>, null]]);
  
recipes.addShaped("lolarecipe47",<potionfingers:ring:1>.withTag({Quality: {}, effect: "minecraft:jump_boost"}),
 [[<minecraft:slime>, <minecraft:rabbit_foot>, null],
  [<minecraft:rabbit_foot>, <potionfingers:ring>, <minecraft:rabbit_foot>],
  [null, <minecraft:rabbit_foot>, null]]);

recipes.addShaped("lolarecipe48",<potionfingers:ring:1>.withTag({Quality: {}, effect: "minecraft:haste"}),
 [[<minecraft:emerald_block>, <minecraft:emerald>, null],
  [<minecraft:emerald>, <potionfingers:ring>, <minecraft:emerald>],
  [null, <minecraft:emerald>, null]]);

recipes.addShaped("lolarecipe49",<potionfingers:ring:1>.withTag({Quality: {}, effect: "minecraft:strength"}),
 [[<minecraft:magma>, <minecraft:blaze_powder>, null],
  [<minecraft:blaze_powder>, <potionfingers:ring>, <minecraft:blaze_powder>],
  [null, <minecraft:blaze_powder>, null]]);

recipes.addShaped("lolarecipe50",<potionfingers:ring:1>.withTag({Quality: {}, effect: "minecraft:regeneration"}),
 [[<minecraft:nether_star>, <minecraft:ghast_tear>, null],
  [<minecraft:ghast_tear>, <potionfingers:ring>, <minecraft:ghast_tear>],
  [null, <minecraft:ghast_tear>, null]]);

recipes.addShaped("lolarecipe51",<potionfingers:ring:1>.withTag({Quality: {}, effect: "minecraft:resistance"}),
 [[<minecraft:diamond_block>, <minecraft:diamond>, null],
  [<minecraft:diamond>, <potionfingers:ring>, <minecraft:diamond>],
  [null, <minecraft:diamond>, null]]);
  
recipes.addShaped("lolarecipe52",<variedcommodities:chain_skirt>,
 [[<craftablechainmail:chainmail_plate>,<ore:string>,<craftablechainmail:chainmail_plate>],
  [<craftablechainmail:chainmail_plate>,null,<craftablechainmail:chainmail_plate>],
  [<craftablechainmail:chainmail_plate>,<craftablechainmail:chainmail_plate>,<craftablechainmail:chainmail_plate>]]);
  
recipes.addShaped("lolarecipe53",<variedcommodities:golden_skirt>,
 [[<minecraft:gold_ingot>,<ore:string>,<minecraft:gold_ingot>],
  [<minecraft:gold_ingot>,null,<minecraft:gold_ingot>],
  [<minecraft:gold_ingot>,<minecraft:gold_ingot>,<minecraft:gold_ingot>]]);
  
recipes.addShaped("lolarecipe54",<variedcommodities:iron_skirt>,
 [[<minecraft:iron_ingot>,<ore:string>,<minecraft:iron_ingot>],
  [<minecraft:iron_ingot>,null,<minecraft:iron_ingot>],
  [<minecraft:iron_ingot>,<minecraft:iron_ingot>,<minecraft:iron_ingot>]]);
  
recipes.addShaped("lolarecipe55",<variedcommodities:diamond_skirt>,
 [[<minecraft:diamond>,<ore:string>,<minecraft:diamond>],
  [<minecraft:diamond>,null,<minecraft:diamond>],
  [<minecraft:diamond>,<minecraft:diamond>,<minecraft:diamond>]]);
  
recipes.addShaped("lolarecipe56",<xat:glowing_ingot>,
 [[<minecraft:glowstone_dust>,<minecraft:blaze_powder>,<minecraft:glowstone_dust>],
  [<minecraft:blaze_powder>,<iceandfire:silver_ingot>,<minecraft:blaze_powder>],
  [<minecraft:glowstone_dust>,<minecraft:blaze_powder>,<minecraft:glowstone_dust>]]);
  
recipes.addShaped("lolarecipe57",<xat:glowing_ingot>,
 [[<xat:glowing_powder>,<xat:glowing_powder>],
  [<xat:glowing_powder>,<xat:glowing_powder>]]);

recipes.addShapeless("lolarecipe59",<variedcommodities:trading_block>,[<minecraft:crafting_table>,<minecraft:crafting_table>]);

recipes.addShaped("lolarecipe60",<xat:damage_shield>,
 [[<xat:glowing_gem>,<ore:logWood>,<xat:glowing_gem>],
  [<ore:logWood>,<minecraft:dragon_egg>|<iceandfire:dragonegg_red>|<iceandfire:dragonegg_green>|<iceandfire:dragonegg_bronze>|<iceandfire:dragonegg_gray>|<iceandfire:dragonegg_blue>|<iceandfire:dragonegg_white>|<iceandfire:dragonegg_sapphire>|<iceandfire:dragonegg_silver>,<ore:logWood>],
  [<xat:glowing_gem>,<ore:logWood>,<xat:glowing_gem>]]);
  
recipes.addShaped("lolarecipe61",<bountifulbaubles:spectralsilt>,
 [[<bountifulbaubles:flare_red>,<bountifulbaubles:flare_red>,<bountifulbaubles:flare_red>],
  [<bountifulbaubles:flare_red>,<bountifulbaubles:disintegrationtablet>,<bountifulbaubles:flare_red>],
  [<bountifulbaubles:flare_red>,<bountifulbaubles:flare_red>,<bountifulbaubles:flare_red>]]);
  
recipes.addShaped("lolarecipe62",<minecraft:sandstone>,
 [[<notreepunching:rock/sandstone>,<notreepunching:rock/sandstone>],
  [<notreepunching:rock/sandstone>,<notreepunching:rock/sandstone>]]);
  
recipes.addShaped("lolarecipe63",<minecraft:red_sandstone>,
 [[<notreepunching:rock/red_sandstone>,<notreepunching:rock/red_sandstone>],
  [<notreepunching:rock/red_sandstone>,<notreepunching:rock/red_sandstone>]]);

print("Script ending!");