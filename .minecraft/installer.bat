@echo off
powershell -Command "Invoke-WebRequest https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.12.2-14.23.5.2854/forge-1.12.2-14.23.5.2854-installer.jar -Outfile forge-1.12.2-14.23.5.2854-installer.jar"
java -jar forge-1.12.2-14.23.5.2854-installer.jar --extract
java -jar forge-1.12.2-14.23.5.2854-installer.jar --installServer
del forge-1.12.2-14.23.5.2854-installer.*
