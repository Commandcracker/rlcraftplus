#!/bin/bash
curl https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.12.2-14.23.5.2854/forge-1.12.2-14.23.5.2854-installer.jar --output forge-1.12.2-14.23.5.2854-installer.jar
java -jar forge-1.12.2-14.23.5.2854-installer.jar --extract
java -jar forge-1.12.2-14.23.5.2854-installer.jar --installServer
rm forge-1.12.2-14.23.5.2854-installer.*
