Run these commands (In console if a server, in game if singleplayer):

/pregen utils setPriority pregenerator
/pregen timepertick 250
/pregen gen startradius square 0 0 b10000

If running a server with less than 4 GB's of RAM, these alternate pregen commands may prevent crashes from too much memory use.  The first set of commands is recommended though:

/pregen utils setPriority pregenerator
/pregen timepertick 250
/pregen gen startmassradius square 0 0 b10000 100

Once finished, restart your game or server.

=========================

YOU SHOULD ALSO SETUP AUTOMATIC RESTARTS FOR AT LEAST EVERY 4 HOURS OR SO AS TO CLEAR THE RAM REGULARLY IF A LOT OF PLAYERS, OR LAG WILL BUILD UP.